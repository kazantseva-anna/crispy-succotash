describe('Feeds Tests', () => {
    before(() => {
        cy.resetDB()
        cy.createTestUsers()
    })

    beforeEach(() => {
        Cypress.Cookies.preserveOnce('sessionid', 'remember_token')
    })

    it('No feeds test', () => {
        cy.login(Cypress.env('USER_1'), Cypress.env('PASSWORD'))
        cy.contains('All Feeds').click()
        cy.getFeedTitles().should('not.exist')
        cy.contains('Nothing to see here. Move on!').should('exist')

        cy.contains('My Feeds').click()
        cy.getFeedTitles().should('not.exist')
        cy.contains('Nothing to see here. Move on!').should('exist')
    })

    it('Invalid feed URL test', () => {
        cy.contains('New Feed').click()

        // empty field
        cy.contains('Submit').click()
        cy.contains('This field is required.').should('exist')

        // string as URL
        cy.addFeed('invalid/url')
        cy.contains('Enter a valid URL.').should('exist')

        //TODO: Uncomment when the bugs are fixed
        // invalid URL
        // cy.addFeed('http://www.invalid/url')
        // cy.contains('Enter a valid URL.').should('exist')

        // valid URL, but RSS is not supported
        // cy.addFeed('https://www.google.com/')
        // cy.contains('Enter a valid URL of the RSS feed.').should('exist')
    })

    it('Add new feed test', () => {
        cy.contains('New Feed').click()
        cy.addFeed(Cypress.env('RSS_URL_1'))

        cy.contains('All Feeds').click()
        cy.getFeedTitles().should('exist')

        cy.contains('My Feeds').click()
        cy.getFeedTitles().should('exist')
 })

    it('Add the same feed again test', () => {
        cy.contains('New Feed').click()
        cy.addFeed(Cypress.env('RSS_URL_1'))
        cy.contains('Feed with this Feed URL already exists.').should('exist')
    })

    it('Add bookmark test', () => {
        cy.contains('Bookmarked').click()
        cy.getFeedTitles().should('not.exist')
        cy.contains('Nothing to see here. Move on!').should('exist')

        cy.contains('All Feeds').click()
        cy.openFirstFeed()
        getBookmarkIcon().click()
        getActiveBookmarkIcon().should('exist')

        cy.contains('Bookmarked').click()
        cy.getFeedTitles().should('exist')
        cy.openFirstFeed()
        getActiveBookmarkIcon().should('exist')
    })

    it('Logged out user test', () => {
        cy.contains('Logout').click()
        cy.contains('My Feeds').should('not.exist')
        cy.contains('Bookmarked').should('not.exist')
        cy.contains('New Feed').should('not.exist')
        cy.contains('Login').should('exist')

        cy.contains('All Feeds').click()
        cy.getFeedTitles().should('exist')
    })

    it('Other users see feeds test', () => {
        cy.login(Cypress.env('USER_2'), Cypress.env('PASSWORD'))

        cy.contains('All Feeds').click()
        cy.getFeedTitles().should('exist')

        cy.contains('My Feeds').click()
        cy.contains('Nothing to see here. Move on!').should('exist')

        cy.contains('Bookmarked').click()
        cy.contains('Nothing to see here. Move on!').should('exist')
    })

    it('Multiple feeds test', () => {
        // user #2 adds a feed
        cy.contains('New Feed').click()
        cy.addFeed(Cypress.env('RSS_URL_2'))

        cy.contains('My Feeds').click()
        cy.getFeedTitles().should('exist')

        cy.contains('All Feeds').click()
        cy.getFeedRows().should(rows => {
            expect(rows).to.have.length(2)
            expect(rows.first()).to.contain(Cypress.env('RSS_URL_1'))
        })

        // login back as user #1
        cy.contains('Logout').click()
        cy.login(Cypress.env('USER_1'), Cypress.env('PASSWORD'))

        cy.contains('All Feeds').click()
        cy.getFeedRows().should(rows => {
            expect(rows).to.have.length(2)
        })
        cy.getFeedRows().eq(1).should('contain', Cypress.env('RSS_URL_2'))
    })

    it('Remove bookmark test', () => {
        cy.contains('Bookmarked').click()
        cy.openFirstFeed()
        getActiveBookmarkIcon().click()
        getBookmarkIcon().should('exist')

        cy.contains('Bookmarked').click()
        cy.getFeedRows().should('not.exist')
        cy.contains('Nothing to see here. Move on!').should('exist');
    })

    function getBookmarkIcon() {
        return cy.get('.glyphicon-heart-empty')
    }

    function getActiveBookmarkIcon() {
        return cy.get('.glyphicon-heart')
    }
})
