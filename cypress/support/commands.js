Cypress.Commands.add('resetDB', () => {
    cy.request('/reset-db')
})

Cypress.Commands.add('signUp', (email, password, passwordConfirmation) => {
    cy.contains('Sign Up').click()
    cy.get('#id_username').type(email)
    cy.get('#id_password1').type(password)
    cy.get('#id_password2').type(passwordConfirmation)
    cy.contains('Submit').click()
})

Cypress.Commands.add('createTestUsers', () => {
    cy.visit('/')
    cy.signUp(Cypress.env('USER_1'), Cypress.env('PASSWORD'), Cypress.env('PASSWORD'))
    cy.contains('Logout').click()
    cy.signUp(Cypress.env('USER_2'), Cypress.env('PASSWORD'), Cypress.env('PASSWORD'))
    cy.contains('Logout').click()
})

Cypress.Commands.add('login', (email, password) => {
    cy.contains('Login').click()
    cy.get('#id_username').type(email)
    cy.get('#id_password').type(password)
    cy.contains('login').click()
})

Cypress.Commands.add('addFeed', url => {
    cy.get('#id_feed_url').type(url)
    cy.contains('Submit').click()
})

Cypress.Commands.add('getFeedRows', () => {
    return cy.get('.table-responsive tbody tr')
})

Cypress.Commands.add('getFeedTitles', () => {
    return cy.get('.table-responsive tr td a')
})

Cypress.Commands.add('openFirstFeed', () => {
    cy.getFeedTitles().first().click()
})
