describe('Login Tests', () => {
    before(() => {
        cy.resetDB()
        cy.createTestUsers()
    })

    beforeEach(() => {
        cy.visit('/')
        cy.contains('Login').click()
    })

    it('Empty fields test', () => {
        cy.contains('login').click()
        cy.get('#error_1_id_username').should('exist')
        cy.get('#error_1_id_password').should('exist')
    })

    it('Invalid username test', () => {
        cy.login(Cypress.env('USER_1').toLowerCase(), Cypress.env('PASSWORD'))
        cy.contains('Please enter a correct username and password. Note that both fields may be case-sensitive.').should('exist')
    })

    it('Invalid password test', () => {
        cy.login(Cypress.env('USER_1'), Cypress.env('PASSWORD').toLowerCase())
        cy.contains('Please enter a correct username and password. Note that both fields may be case-sensitive.').should('exist')
    })

    it('Login test', () => {
        cy.login(Cypress.env('USER_1'), Cypress.env('PASSWORD'))
        cy.contains('My Feeds').should('exist')
        cy.contains('Bookmarked').should('exist')
        cy.contains('New Feed').should('exist')
        cy.contains('Logout').click()
    })
})
