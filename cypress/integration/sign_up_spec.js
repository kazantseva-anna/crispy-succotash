describe('Sign Up Tests', () => {

    before(() => {
        cy.resetDB()
    })

    beforeEach(() => {
        cy.visit('/')
        cy.contains('Sign Up').click()
    })

    it('Empty fields test', () => {
        cy.contains('Submit').click()
        cy.get('#error_1_id_username').should('exist')
        cy.get('#error_1_id_password1').should('exist')
        cy.get('#error_1_id_password2').should('exist')
        cy.contains('Required. 30 characters or fewer. Letters, digits and @/./+/-/_ only.').should('exist')
        cy.contains('Enter the same password as before, for verification.').should('exist')
    })

    it('Invalid username test', () => {
        cy.signUp('Invalid!?/', Cypress.env('PASSWORD'), Cypress.env('PASSWORD'))
        cy.contains('Required. 30 characters or fewer. Letters, digits and @/./+/-/_ only.').should('exist')
        cy.contains('Enter a valid username. This value may contain only letters, numbers and @/./+/-/_ characters.').should('exist')
    })

    it('Invalid password test', () => {
        cy.signUp(Cypress.env('USER_1'), '123', '123')
        cy.contains('This password is too short. It must contain at least 8 characters.').should('exist')
        cy.contains('This password is entirely numeric.').should('exist')
        cy.contains('Enter the same password as before, for verification.').should('exist')
    })

    it('Passwords do not match test', () => {
        cy.signUp(Cypress.env('USER_1'), '12345678a', '12345678b')
        cy.contains("The two password fields didn't match.").should('exist')
        cy.contains('Enter the same password as before, for verification.').should('exist')
    })

    it('Sign up test', () => {
        cy.signUp(Cypress.env('USER_1'), Cypress.env('PASSWORD'), Cypress.env('PASSWORD'))
        cy.contains('My Feeds').should('exist')
        cy.contains('Bookmarked').should('exist')
        cy.contains('New Feed').should('exist')
        cy.contains('Logout').click()
    })

    it('Sign up existing user test', () => {
        cy.contains('Sign Up').click()
        cy.signUp(Cypress.env('USER_1'), 'new password', 'new password')
    })
})
