# Crispy Succotash
## Installation

1. Install [Docker](https://docs.docker.com/installation/) and [Docker Compose](https://docs.docker.com/compose/install/)

2. cd into the project directory
  
  ```shell
  cd crispy-succotash
  ```
  
  2.1. **SKIP THIS STEP IF YOU'RE USING LINUX**
    
    If you're running Docker in a VM  (docker-machine) you'll find permission issues due to the way Postgres' handles its data directory permissions and the fact that you're running it inside a VM with a shared AUFS folder from your host (bug description can be found [on this link](https://github.com/docker-library/postgres/issues/60)). There are ways to fix this, but they take time and are quite annoying. 
    
    I've created a separated docker-compose file for Macs, which needs to be specified when running all docker-compose commands. 
    You can run the project directly with no preparation commands.
    


  2.2. **SKIP THIS STEP IF YOU'RE USING DOCKER MACHINE**
  
  Create your database folder
      
  ```shell
  mkdir -p tmp/pgdata
  ```
  
  Now you need to run the database separately before we can run the apps for the first time so the users are created

  ```shell
  docker-compose run db
  ```
  Once it finishes, exit the process (Ctrl+C!) and proceed to the next step
  
3. Run it

  Linux

  ```shell
  docker-compose up
  ```
  
  Docker-Machine

  ```shell
  docker-compose -f docker-compose.mac.yml up
  ```
  
That's it! :) The project will be available on port 8000 of your Docker host. 

## Running unit and integration tests

  ```shell
  docker-compose run app bash
  ./manage.py test
  ```

## End-to-end tests (Cypress)

### Decision records and assumptions
1. All Cypress tests are end-to-end. It's a small app and test execution time shouldn't be a problem. For bigger applications it's possible to use a combination of e2e tests and tests that [stub network requests](https://docs.cypress.io/guides/guides/network-requests.html#Stubbing).
1. Tests are organized into multiple independent spec files. The database is reset before execution of each spec file.
1. In order to reset the database I've created a `/reset-db` endpoint in the app which is available only when `CRISPY_DB_RESET_ENDPOINT` environment variable is set to `1`.
1. Since a number of tests is small there's no test framework or [Page Objects](https://www.cypress.io/blog/2019/01/03/stop-using-page-objects-and-start-using-app-actions/). Shared test helpers can be found in `cypress/support/commands.js`. Helpers specific to a single spec file are typically defined in the file itself.
1. I used the existing layout to find elements in tests. It could be fine for small apps but in most cases it's better to add `data-*` elements, which is considered one of [best practices](https://docs.cypress.io/guides/references/best-practices.html#Selecting-Elements) in Cypress.
1. [GitLab CI pipeline](https://gitlab.com/kazantseva-anna/crispy-succotash/-/pipelines) runs tests using the Docker-in-Docker approach. This makes it easier to test the pipeline locally before applying changes to GitLab CI.

### Prequisites
- [Node.js 10+, npm, npx](https://nodejs.org/en/download/)

### Running e2e tests via command line
```shell
npm install          # install dependencies (cypress itself)
docker-compose up -d # run the app
npx cypress run      # run tests
```

### Running e2e tests in Docker
GitLab CI pipeline uses `docker-compose` to run tests. This approach can also be used locally to test the pipeline.

```shell
# run the app + tests and exit when tests are finished

docker-compose -f docker-compose.cypress.yml up --exit-code-from cypress
```

### Running e2e tests manually via Cypress Test Runner
When developing locally it's convenient to use [Cypress Test Runner](https://docs.cypress.io/guides/core-concepts/test-runner.html#Overview).

```shell
npx cypress open
```
