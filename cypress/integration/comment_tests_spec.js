describe('Comments Tests', () => {
    let feedEntryUrl

    before(() => {
        cy.resetDB()
        cy.createTestUsers()
        cy.login(Cypress.env('USER_1'), Cypress.env('PASSWORD'))
        cy.contains('New Feed').click()
        cy.addFeed(Cypress.env('RSS_URL_1'))
    })

    beforeEach(() => {
        Cypress.Cookies.preserveOnce('sessionid', 'remember_token')
    })

    it('Add comments test', () => {
        cy.openFirstFeed()
        cy.url().then(url => feedEntryUrl = url)
        cy.get('.col-lg-12.well').should('exist')

        // add comment with markdown syntax
        addComment('**Bold** *italic*')
        getComments().should('contain', 'Bold italic')
        getComments().should('not.contain', '*')
        cy.contains(Cypress.env('USER_1')).should('exist')

        // add picture in a comment
        cy.get('img').should('not.exist')
        addComment('![](https://upload.wikimedia.org/wikipedia/commons/7/79/Face-smile.svg)')
        cy.get('img').should('exist')
        
        cy.contains('Logout').click()
    })

    it('See comments after logout test', () => {
        cy.visit(feedEntryUrl)
        cy.contains('Sign in to comment').should('exist')

        getComments().should('contain', 'Bold italic')
        getComments().should('not.contain', '*')
        cy.get('img').should('exist')
        cy.contains(Cypress.env('USER_1')).should('exist')
    })

    it('Other users see comments test', () => {
        cy.contains('Login').click()
        cy.login(Cypress.env('USER_2'), Cypress.env('PASSWORD'))

        cy.visit(feedEntryUrl)
        getComments().should('contain', 'Bold italic')
        getComments().should('not.contain', '*')
        cy.get('img').should('exist')
        cy.contains(Cypress.env('USER_1')).should('exist')
    })

    function addComment(text) {
        cy.get('.CodeMirror textarea').type(text, { force: true })
        cy.contains('Submit').click()
    }

    function getComments() {
        return cy.get("[id*='comment']")
    }
})
